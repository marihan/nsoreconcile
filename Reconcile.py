import ncs

#open session with NSO
def open_session():
    session_username = "admin"
    session_context = 'Python test script '
    m = ncs.maapi.Maapi()
    s = ncs.maapi.Session(m, session_username, session_context)
    t = m.start_write_trans()

    return t





def get_prefix_set(device_name,prefix_name,t):
    values={}
    key="reconcile-comment"
    values[key]=[]
    root = ncs.maagic.get_root(t)
    #root.devices.device[device_name].config.cisco_ios_xr__prefix_set.create(prefix_name)
    #print("get config")
    #print(prefix_name)
    try:
        prefixlist = root.devices.device[device_name].config.cisco_ios_xr__prefix_set[prefix_name]

        for item in prefixlist.set:
            #print(item.value)
            if (item.value.startswith('#')) :   
                values[item.value]=[]
                key=item.value
            else:
                values[key].append(item.value)

        if not values['reconcile-comment']:
            del values['reconcile-comment']
      
        return {"dic":values ,"lastkey":key}

    except Exception as e:
        print("enter Exception transaction prefixset not found")
        return {"dic":values ,"lastkey":key}


def get_all_prefixset(device_name,prefix_name,t):
    prefix_name_list = []
    result={}
    keys=""
    root = ncs.maagic.get_root(t)
    if (prefix_name == 'all'):
        for device in root.devices.device:
            if (device.name == device_name):
                for prefix in device.config.cisco_ios_xr__prefix_set:
                    prefix_name_list.append(prefix.name)
    else:
        prefix_name_list.append(prefix_name)

    for index in prefix_name_list:
        result[index]=get_prefix_set(device_name,index,t)["dic"]

    return result


def CreateService():
    global Servicepre
    newT = open_session()
    # with ncs.maapi.single_write_trans('admin', 'admin', groups=['ncsadmin']) as t:
    #     root = ncs.maagic.get_root(t)
    root = ncs.maagic.get_root(newT)
    Servicepre = root.prefix_set_list__prefix_set_list
    return newT


def createPrefixSet(prefixset_name, device, newcomments,t):
    ls_created_services=[]
    prefixset_dic=get_prefix_set(device,prefixset_name,t)
    print("---------------------old dict -----------------------------")
    print(prefixset_dic["dic"])
    print("---------------------new dict -----------------------------")
    print(newcomments)
    if 'empty' in newcomments:
        prefixset_dic["dic"][prefixset_dic["lastkey"]].append(newcomments.pop('empty'))
    prefixset_dic["dic"].update(newcomments)

    print("--------------------- updated dict -----------------------------")
    print(prefixset_dic["dic"])
    global Servicepre
    service=Servicepre.create(device, prefixset_name)
    ls_created_services.append(service)
    print(ls_created_services)

    for comment,ips in prefixset_dic["dic"].items():
        if (comment!=''):
            if (comment.startswith('#')):
                comment_entry=service.configrations.create(comment)
            else:
                comment_entry=service.configrations.create('#'+comment)

            comment_entry.ip_prefix=ips

    #return ls_created_services


def commitService():

    global Servicepre

    print("enter commit changes")
    try:
        Servicepre._apply(True,16)
        #Servicepre._apply((True, _ncs.maapi.COMMIT_NCS_NO_NETWORKING)
        Servicepre=None
        s= "successful"
        return True , s
    except Exception as e:
        #trans.revert()
        Servicepre=None
        print("enter Exception transaction")
        print(str(e))
        return False ,str(e)

def prefix_set_serviceDiscover(device):
    t=open_session()
    prefixs_sets_config=get_all_prefixset(device,"all",t)
    root = ncs.maagic.get_root(t)
    CreateService()
    services_already_exist=[]
    #for service in root.prefix_set_list__prefix_set_list:
        #services_already_exist.append(service.prefix_set_name)
    
    #print(services_already_exist)
    for prefixset in prefixs_sets_config :
        print(prefixset)
        #if (prefixset not in services_already_exist):
        print("create new service")
        print(prefixset)
        print(prefixs_sets_config[prefixset])
        createPrefixSet(prefixset, device, prefixs_sets_config[prefixset],t)

    result , msg =commitService()

    print(result)
    #return ls_services

def prefix_set_reconcile_redeploy(deviceName):
    t=open_session()
    root = ncs.maagic.get_root(t)
    for service in root.prefix_set_list__prefix_set_list:
        if(service.device == deviceName):
            print(service.prefix_set_name)
            inputs = service.re_deploy.get_input()
            inputs.reconcile.create()
            service.re_deploy(inputs)


def prefix_set_reconcile_dryrun(deviceName):
    t=open_session()
    root = ncs.maagic.get_root(t)
    for service in root.prefix_set_list__prefix_set_list:
        if(service.device == deviceName):
            print(service.prefix_set_name)
            action = service.re_deploy
            input = action.get_input()
            input.reconcile.create()
            input.dry_run.outformat= "native"
	    input.dry_run.create()
	    #input.outformat ="native"
	    output = action(input)
            #print(output.native.device['cisco-XRV2-read-only'].data)
            devlist = output.native.device
	   
	    for dev in devlist :
		print("enter")
                print('tryAction: Dry-run device: ', dev.name)

                print('tryAction: Dry-run data: ', dev.data)


            

# to check success of reconcile 
#show full-configuration devices device cisco config prefix-set | display service-meta-data

# first action to do before reconcile is sync from
 

#prefix_set_serviceDiscover("cisco-XRV)
prefix_set_reconcile_dryrun("cisco-XRV)
#prefix_set_reconcile_redeploy("cisco-XRV")



